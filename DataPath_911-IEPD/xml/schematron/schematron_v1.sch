<?xml version="1.0" encoding="UTF-8"?>
<!-- Schematron business rules for DataPath IEPD
     Version 1.0 -->
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <title>DataPath 911 Tests</title>
	
	<ns prefix="x" uri="http://www.w3.org/TR/REC-html40"/>
    <ns prefix="dp911ext" uri="http://www.911.gov/data-path-911-ext/1.0"/>
    <ns prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
    <ns prefix="nc" uri="http://release.niem.gov/niem/niem-core/4.0/"/>
    <ns prefix="dp911xcg" uri="http://www.911.gov/data-path-911-exchange/1.0"/>
    <ns prefix="em" uri="http://release.niem.gov/niem/domains/emergencyManagement/4.2/"/>
	<ns prefix="it" uri="http://release.niem.gov/niem/domains/internationalTrade/4.2/"/>
	<ns prefix="j" uri="http://release.niem.gov/niem/domains/jxdm/6.2/"/>    
    
    
    <!-- There can only exist one of contactEmail and contactPhone in the ContactInformation node, contained in the header -->
    <pattern id="Header">
        <rule context="//dp911xcg:DataPath911RequestMessage">
            <report test="count(./em:EMMessage/dp911ext:EMMessageAugmentation/nc:ContactInformation/nc:ContactEmailID) > 1">The <name/> must not contain more than one ContactEmailID element.</report>
            <report test="count(./em:EMMessage/dp911ext:EMMessageAugmentation/nc:ContactInformation/nc:ContactTelephoneNumber) > 1">The <name/> must not contain more than one ContactEmailID element.</report>
        </rule>
    </pattern>
    
    
   

</schema>
